--liquibase formatted sql

--changeset aboba:1
CREATE TABLE IF NOT EXISTS employees
(
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email_id VARCHAR(255)
);
--changeset aboba:2
INSERT INTO employees(first_name, last_name, email_id)
VALUES('GOSHA', 'GOGAVICH', 'aboba');